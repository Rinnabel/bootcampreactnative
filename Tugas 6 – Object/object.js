//Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    // Code di sini
    if (arr.length <= 0) {
        return console.log("")
    }

    for (i = 0; i < arr.length; i++) {
        var biodata = {}
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)
        var umur;

        if (thisYear - arr[i][3] > 0 && arr[i][3] > 0) {
            umur = thisYear - arr[i][3];
        } else {
            umur = 'Invalid birth year'
        }

        biodata.firstName = arr[i][0];
        biodata.lastName = arr[i][1];
        biodata.gender = arr[i][2];
        biodata.age = umur;

        var consoleText = (i + 1) + '. ' + biodata.firstName + ' ' + biodata.lastName + ': '
        console.log(consoleText)
        console.log(biodata)
    }
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)

// Error case 
arrayToObject([]) // ""

console.log("\n")
//Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    if (!memberId) {
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        console.log("Mohon maaf, uang tidak cukup")
    } else {
        var newObject = {};
        var moneyChange = money;
        var purchasedList = [];

        var sepatu = 'Sepatu Stacattu';
        var baju1 = 'Baju Zoro';
        var baju2 = 'Baju H&N';
        var jaket = 'Sweater Uniklooh';
        var casing = 'Casing Handphone';

        var check = 0;

        for (var i = 0; moneyChange >= 50000 && check == 0; i++) {
            if (moneyChange >= 1500000) {
                purchasedList.push(sepatu)
                moneyChange -= 1500000
            } else if (moneyChange >= 500000) {
                purchasedList.push(baju1)
                moneyChange -= 500000
            } else if (moneyChange >= 250000) {
                purchasedList.push(baju2)
                moneyChange -= 250000
            } else if (moneyChange >= 175000) {
                purchasedList.push(jaket)
                moneyChange -= 175000
            } else if (moneyChange >= 50000) {

                if (purchasedList.length != 0) {
                    for (var j = 0; j <= purchasedList.length - 1; j++) {
                        if (purchasedList[j] == casing) {
                            check += 1
                        }
                    }
                    if (check == 0) {
                        purchasedList.push(casing)
                        moneyChange -= 50000
                    }
                } else {
                    purchasedList.push(casing)
                    moneyChange -= 50000
                }

            }
        }

        newObject.memberId = memberId
        newObject.money = money
        newObject.listPurchased = purchasedList
        newObject.changeMoney = moneyChange

        return newObject
    }

}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\n")
//Soal No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var arrOutput = []
    if (arrPenumpang.length <= 0) {
        return []
    }

    for (var x = 0; x < arrPenumpang.length; x++) {
        var objOutput = {}
        // var arrOrang = arrPenumpang[i]
        var asal = arrPenumpang[x][1]
        var tujuan = arrPenumpang[x][2]

        var indexAsal;
        var indexTujuan;

        for (var y = 0; y < rute.length; y++) {
            if (rute[y] == asal) {
                indexAsal = y
            } else if (rute[y] == tujuan) {
                indexTujuan = y
            }
        }

        var bayar = (indexTujuan - indexAsal) * 2000
        // var bayar = Math.abs(indexTujuan - indexAsal) * 2000

        objOutput.penumpang = arrPenumpang[x][0]
        objOutput.naikDari = asal
        objOutput.tujuan = tujuan
        objOutput.bayar = bayar

        arrOutput.push(objOutput)
    }

    return arrOutput
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]