import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Login extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.garis} />

                {/* Isi  */}
                <View style={styles.isi}>
                    <Text style={styles.nama}>Adeel Ela</Text>
                    <Text style={styles.information}>React Native Developer</Text>
                    <View style={{ alignItems: 'center' }}>
                        <View style={styles.kotakGrey}>
                            <Text style={styles.textinKotak}>PORTOFOLIO</Text>
                            <View style={styles.garis1} />
                        </View>
                        <View style={styles.kotakGrey}>
                            <Text style={styles.textinKotak}>CONTACT</Text>
                            <View style={styles.garis1} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : { padding: 30, marginTop: 50, },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    garis: {
        borderBottomWidth: 1,
        marginTop: 10
    },
    isi: {
        marginTop: 30,
    },

    kotakGrey: {
        width: 300,
        height: 100,
        backgroundColor: '#EFEFEF',
        marginBottom: 10,
    },
    nama: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom: 5,
    },
     information: {
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom: 15,
    },
    textinKotak: {
        padding: 5,
    },
     garis1: {
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
    },
     
})