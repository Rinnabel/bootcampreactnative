//No. 1 Looping While 

console.log('LOOPING PERTAMA')
var i = 2
while (i <= 20) {
    console.log(i + ' - I love coding');
    i += 2;
}

console.log('LOOPING KEDUA')
var x = 20
while (x > 2) {
    console.log(x + ' - I will become a mobile developer');
    x -= 2;
}

//No. 2 Looping menggunakan for

for (var y = 1; y <= 20; y++) {
    if (y % 3 == 0 && y % 2 != 0) {
        console.log(y + ' - I Love Coding')
    } else if (y % 2 == 0) {
        console.log(y + ' - Berkualitas')
    } else {
        console.log(y + ' - Santai')
    }
}

//No. 3 Membuat Persegi Panjang #

i = 1;
var j = 1;
var panjang = 8;
var lebar = 4;
var pagar = '';

while (j <= lebar) {
  while (i <= panjang) {
    pagar += '#';
    i++;
  }
  console.log(pagar);
  pagar = '';
  i = 1;
  j++;
}


//No. 4 Membuat Tangga 

var tangga = '#'
for (var a = '#'; a.length <= 7; a += tangga) {
    console.log(a)
}

//No. 5 Membuat Papan Catur

var panjang = 8;
var lebar = 8;
var papan = " ";

for (y = 1; y <= lebar; y++) {
    if (y % 2 == 1) {
        for (x = 1; x <= panjang; x++) {
            if (x % 2 == 1) {
                papan += " "
            } else {
                papan += "#"
            }
        }
    } else {
        for (x = 1; x <= panjang; x++) {
            if (x % 2 == 1) {
                papan += "#"
            } else {
                papan += " "
            }
        }
    }
    console.log(papan);
    papan = " ";
}