//Soal No. 1 (Range) 

function range(startNum, finishNum) {
    var angka = [];
    if (startNum < finishNum) {
        var rangeArray = finishNum - startNum + 1;
        for (i = 0; i < rangeArray; i++) {
            angka.push(startNum + i);
        }
    } else if (startNum > finishNum) {
        var rangeArray = startNum - finishNum + 1;
        for (i = 0; i < rangeArray; i++) {
            angka.push(startNum - i);
        }
    } else if (!startNum || !finishNum) {
        return -1
    }
    return angka
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("\n")
//Soal No. 2 (Range with Step)

function rangeWithStep(startNum, finishNum, step) {
    var angka = [];
    if (startNum < finishNum) {
        var rangeArray = finishNum - startNum + 1;
        for (i = 0; i < rangeArray; i += step) {
            angka.push(startNum + i);
        }
    } else if (startNum > finishNum) {
        var rangeArray = startNum - finishNum + 1;
        for (i = 0; i < rangeArray; i += step) {
            angka.push(startNum - i);
        }
    } else if (!startNum || !finishNum) {
        return -1
    }
    return angka
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\n")
//Soal No. 3 (Sum of Range)

function sum(startNum, finishNum, step) {
    var angka = []
    if (!step) {
        step = 1
    }

    if (startNum < finishNum) {
        var rangeArray = finishNum - startNum + 1;
        for (i = 0; i < rangeArray; i += step) {
            angka.push(startNum + i);
        }
    } else if (startNum > finishNum) {
        var rangeArray = startNum - finishNum + 1;
        for (i = 0; i < rangeArray; i += step) {
            angka.push(startNum - i);
        }
    } else if (!startNum && !finishNum && !step) {
        return 0
    } else if (startNum) {
        return startNum
    }

    var total = 0
    for (i = 0; i < angka.length; i++) {
        total = total + angka[i];
    }
    return total
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\n")
//Soal No. 4 (Array Multidimensi)

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], //0
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], //1
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], //2
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] //3
]

function dataHandling(arrayMulti) {
    arrayLength = arrayMulti.length
    for (i = 0; i < arrayLength; i++) {
        var id = "Nomor ID: " + arrayMulti[i][0];
        var nama = "Nama Lengkap: " + arrayMulti[i][1];
        var ttl = "TTL: " + arrayMulti[i][2] + " " + arrayMulti[i][3];
        var hobi = "Hobi: " + arrayMulti[i][4];

        console.log(id);
        console.log(nama);
        console.log(ttl);
        console.log(hobi);
        console.log();
    }
}

dataHandling(input);

console.log("\n")
//Soal No. 5 (Balik Kata)

function balikKata(kata) {
    kataBaru = '';
    kataLength = kata.length - 1
    for(i=kataLength; i>=0; i--) {
        kataBaru += (kata[i]);
    }
    return kataBaru
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("\n")
//Soal No. 6 (Metode Array)

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(data2) {
    data2.splice(1, 1, data2[1] + " Elsharawy");
    data2.splice(2, 1, "Provinsi " + data2[2]);
    data2.splice(4, 0, "Pria");
    data2.splice(5, 1, "SMA Internasional Metro");
    console.log(data2);

    var date = data2[3].split("/"); // ['21', '05', '1989'];
    var bulan = date[1];

    switch (bulan) {
        case '01':
            bulan = "Januari"
            break;
        case '02':
            bulan = "Februari"
            break;
        case '03':
            bulan = "Maret"
            break;
        case '04':
            bulan = "April"
            break;
        case '05':
            bulan = "Mei"
            break;
        case '06':
            bulan = "Juni"
            break;
        case '07':
            bulan = "Juli"
            break;
        case '08':
            bulan = "Agustus"
            break;
        case '09':
            bulan = "September"
            break;
        case '10':
            bulan = "Oktober"
            break;
        case '11':
            bulan = "Nopember"
            break;
        case '12':
            bulan = "Desember"
            break;
        default:
            break;
    }
    console.log(bulan);

    var dateSort = date.sort(function (value1, value2) { return value2 - value1 });
    console.log(dateSort);

    var dateJoin = date.join('-');
    console.log(dateJoin)

    var namaBaru = data2[1].slice(0,14);
    console.log(namaBaru);
}

dataHandling2(input2);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 